import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;

import javax.servlet.ServletContext;

import org.zkoss.bind.annotation.BindingParam;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.util.media.Media;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.WebApps;

import com.sun.xml.internal.messaging.saaj.util.ByteOutputStream;


public class UploadImageViewModel {
	//Este atributo representa el archivo que se esta subiendo
	private Media uploadedImage;

	public Media getUploadedImage() {
		return uploadedImage;
	}

	public void setUploadedImage(Media uploadedImage) {
		this.uploadedImage = uploadedImage;
	}
	
	/*
	 * Este metodo es el que sube la imagen al servidor, recibe la imagen como parametro y la guarda en uploadedImage
	 * Luego obtiene la direccion donde debe guardar la imagen
	 * Crea un nuevo archivo en la ruta que se consiguio con el mismo nombre que tenia originalmente
	 * 
	 * */
	@Command
	@NotifyChange("uploadedImage")
	public void upload(@BindingParam("media") Media myMedia){
		uploadedImage = myMedia;
		String path = WebApps.getCurrent().getServletContext().getInitParameter("upload.location");
		File imageFile = new File(path,uploadedImage.getName());
			
			try {
				//Este codigo es el que sube la imagen
				InputStream is = uploadedImage.getStreamData();
				Files.copy(is, imageFile.toPath());
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		
		/*BufferedInputStream bis = null;
		BufferedOutputStream bos = null;
		InputStream is = uploadedImage.getStreamData();
		bis = new BufferedInputStream(is);
		try {
			imageFile.createNewFile();
			bos = new BufferedOutputStream(new FileOutputStream(imageFile));
			byte buffer[] = new byte[1024];
			int ch = bis.read();
			while(ch != -1){
				bos.write(buffer, 0, ch);
				ch = bis.read();
			}
			
			if(bis != null)
				bis.close();
			if(bos != null)
				bos.close();
			if(is != null){
				is.close();
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally{
			
		}	*/	
	}
}
