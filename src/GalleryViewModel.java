import java.io.File;
import java.io.FilenameFilter;

import org.zkoss.zk.ui.WebApps;


public class GalleryViewModel {
	private String[] imagesPaths;
	
	public GalleryViewModel() {
		String path = WebApps.getCurrent().getServletContext().getInitParameter("upload.location");
		File f = new File(path);
		imagesPaths = f.list();
	}

	public String[] getImagesPaths() {
		return imagesPaths;
	}

	public void setImagesPaths(String[] imagesPaths) {
		this.imagesPaths = imagesPaths;
	}
}
